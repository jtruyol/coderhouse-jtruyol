import React from 'react'; 
import './App.css';

import Navbar from './components/Navbar';

function App() {
  return (
    <div className="">
      <header className="">
        <Navbar/>
      </header>
      <footer>
        <div>
          Icons made by 
          <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons
          </a> from 
          <a href="https://www.flaticon.com/" title="Flaticon">
            www.flaticon.com
          </a>
        </div> 
      </footer>
    </div>
  );
}

export default App;
