import React from 'react';
import Nav from 'react-bootstrap/Nav';
import {Navbar as BtNavbar} from 'react-bootstrap';

import logo from './../logo.svg';

const Navbar = () => {
    return (
        <BtNavbar fluid={ 'true' }>
            <BtNavbar.Brand href="#home">
                <img
                    src={logo}
                    width="30"
                    height="30"
                    className="d-inline-block align-top"
                    alt="Logo Auxiliar"
                />{' '}
                Cafe Colao
            </BtNavbar.Brand>
            <Nav className="mr-auto">
                <Nav.Link href="#home">Home</Nav.Link>
                <Nav.Link href="#grano">Café en grano</Nav.Link>
                <Nav.Link href="#molido">Café molido</Nav.Link>
                <Nav.Link href="#instantaneo">Café instantaneo</Nav.Link>
                <Nav.Link href="#accesorios">Accesorios</Nav.Link>
            </Nav>
        </BtNavbar>
    );
}

export default Navbar;